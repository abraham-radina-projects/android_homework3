package com.example.databaseapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.databaseapp.R;
import com.example.databaseapp.adapters.AdapterBook;
import com.example.databaseapp.interfaces.ActivitiesFragmentsCommunication;
import com.example.databaseapp.interfaces.OnBookItemClick;
import com.example.databaseapp.models.Book;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class BookAppFragment extends Fragment {
    public static final String TAG_BOOK="TAG_BOOK";

    private ActivitiesFragmentsCommunication fragmentCommunication;
    ArrayList<Book> books=new ArrayList<Book>();

    AdapterBook bookAdapter=new AdapterBook(books, new OnBookItemClick() {
        @Override
        public void onClick(Book book) {
            if (fragmentCommunication != null) {
                fragmentCommunication.onReplaceSecondFragment(book);
                Toast.makeText(getContext(), "Fragment 2", Toast.LENGTH_SHORT).show();
            }
        }
    });

    public static BookAppFragment newInstance() {

        Bundle args = new Bundle();
        BookAppFragment fragment = new BookAppFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_book_app,container,false);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        RecyclerView userList = (RecyclerView) view.findViewById(R.id.book_list);
        userList.setLayoutManager(linearLayoutManager);
        books.clear();
        books.add(new Book("Golden Girl", "Elin Hilderbrand","Best Literature & Fiction"));
        books.add(new Book("People We Meet on Vacation", "Emily Henry","New Adult"));
        books.add(new Book("Malibu Rising", "Taylor Jenkins Reid "," Sisters Fiction"));
        userList.setAdapter(bookAdapter);

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if(context instanceof ActivitiesFragmentsCommunication){
            fragmentCommunication=(ActivitiesFragmentsCommunication) context;
        }
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        view.findViewById(R.id.btn_book).setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

              EditText ed=(EditText) view.findViewById(R.id.edt_title);
              String title=ed.getText().toString();
                EditText ed2=(EditText) view.findViewById(R.id.edt_author);
                String author=ed2.getText().toString();
                EditText ed3=(EditText) view.findViewById(R.id.edt_description);
                String description=ed3.getText().toString();
                Toast.makeText(getContext(),"Click",Toast.LENGTH_SHORT).show();
                ed.setText("");
                ed2.setText("");
                ed3.setText("");
            }
        });


    }
}
